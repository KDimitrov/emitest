package com.telerikacademy.oop.agency.core.contracts;

public interface Reader {
    
    String readLine();
    
}