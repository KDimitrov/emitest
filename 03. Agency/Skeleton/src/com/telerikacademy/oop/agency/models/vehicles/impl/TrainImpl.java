package com.telerikacademy.oop.agency.models.vehicles.impl;

import com.telerikacademy.oop.agency.models.common.VehicleType;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Train;
import com.telerikacademy.oop.agency.utils.ValidationHelper;

public class TrainImpl extends VehicleImpl implements Train {

    private static final int MIN_PASSENGER = 30;
    private static final int MAX_PASSENGER = 150;
    private static final String PASSENGER_ERROR_MSG = String.format(
            "Number of passengers must be between %d and %d.",
            MIN_PASSENGER,
            MAX_PASSENGER);

    private static final int MIN_CARTS = 1;
    private static final int MAX_CARTS = 15;
    private static final String CARTS_ERROR_MSG = String.format(
            "Carts must be between %d and %d.",
            MIN_CARTS,
            MAX_CARTS);

    private static final double MIN_PPK = 0.1;
    private static final double MAX_PPK = 2.5;
    private static final String PPK_ERROR_MSG = String.format(
            "Price per kilometer must be between %d and %d.",
            MIN_PPK,
            MAX_PPK);

    private int cartCount;


    public TrainImpl(int passengerCapacity, double pricePerKilometer, VehicleType vehicleType, int cartCount) {
        super(passengerCapacity, pricePerKilometer, vehicleType);

        setCartCount(cartCount);

    }

    private void setCartCount(int cartCount) {
        ValidationHelper.validateCartCount(cartCount, MIN_CARTS, MAX_CARTS);
        this.cartCount = cartCount;

    }

    @Override
    protected void validatePassengerCapacity(int value) {
        ValidationHelper.validatePassengerCapacity(value, MIN_PASSENGER, MAX_PASSENGER);
    }

    @Override
    protected void validateCartCount(int value) {

    }

    @Override
    protected void validatePricePerKilometer(double pricePerKilometer) {
        ValidationHelper.validatePricePerKilometer(pricePerKilometer, MIN_PPK, MAX_PPK);
    }

    @Override
    public int getCarts() {
        return cartCount;
    }
}