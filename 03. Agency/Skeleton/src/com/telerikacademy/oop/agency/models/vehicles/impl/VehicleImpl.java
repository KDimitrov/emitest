package com.telerikacademy.oop.agency.models.vehicles.impl;

import com.telerikacademy.oop.agency.models.common.VehicleType;
import com.telerikacademy.oop.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleImpl implements Vehicle {

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType vehicleType;

    public VehicleImpl(int passengerCapacity, double pricePerKilometer, VehicleType vehicleType) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(vehicleType);
    }

    protected  void setType(VehicleType vehicleType){
        this.vehicleType = vehicleType;
    }

    @Override
    public VehicleType getType(){
        return vehicleType;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }
    protected abstract void validatePassengerCapacity(int value);

    public void setPassengerCapacity(int passengerCapacity) {
        validatePassengerCapacity(passengerCapacity);
        this.passengerCapacity = passengerCapacity;
    }

    protected abstract void validateCartCount(int value);

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public void setPricePerKilometer(double pricePerKilometer) {
        this.pricePerKilometer = pricePerKilometer;
    }

    protected abstract void validatePricePerKilometer(double pricePerKilometer);
}
