package com.telerikacademy.oop.agency.utils;

public class ValidationHelper {

    public static void validatePassengerCapacity(int value, int min, int max) {
        validateObjectExists(value);

        if (value < min || value > max) {
            throw new IllegalArgumentException(String.format("Passenger count must be between %d and %d.", min, max));
        }

    }


    public static void validateCartCount(int value, int min, int max) {
        validateObjectExists(value);

        if (value < min || value > max) {
            throw new IllegalArgumentException(String.format("Cart count must be between %d and %d.", min, max));
        }

    }

    public static void validatePricePerKilometer(double value, double min, double max) {
        validateObjectExists(value);

        if (value < min || value > max){
            throw new IllegalArgumentException(String.format("PPK must be between %d and %d.", min, max));
        }
    }


    private static void validateObjectExists(Object object) {
        if (object == null) {
            throw new IllegalArgumentException("Object cannot be null");
        }
    }
}